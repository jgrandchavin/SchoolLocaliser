package com.juliengrandchavin.schoollocaliser.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.juliengrandchavin.schoollocaliser.R;

public class MenuActivity extends AppCompatActivity {

    // Components declarations
    public ImageButton mapButton;
    public ImageButton listButton;
    public ImageButton addToListButton;
    public ImageButton settingsButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Components initialization
        mapButton = findViewById(R.id.map_button);
        mapButton.setOnClickListener(genericOnClickListener);

        listButton = findViewById(R.id.list_button);
        listButton.setOnClickListener(genericOnClickListener);

        addToListButton = findViewById(R.id.add_to_list_button);
        addToListButton.setOnClickListener(genericOnClickListener);

        settingsButton = findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(genericOnClickListener);

    }

    // OnClickListener for this view
    View.OnClickListener genericOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.map_button:
                    // Go to MapActivity
                    Intent intent = new Intent(MenuActivity.this, MapActivity.class);
                    startActivity(intent);
                    break;
                case R.id.list_button:
                    // Go to MapActivity
                    intent = new Intent(MenuActivity.this, SchoolListActivity.class);
                    startActivity(intent);
                    break;
                case R.id.add_to_list_button:
                    // Go to MapActivity
                    intent = new Intent(MenuActivity.this, AddToListActivity.class);
                    startActivity(intent);
                    break;
                case R.id.settings_button:
                    // Go to MapActivity
                    intent = new Intent(MenuActivity.this, SettingsActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };
}
