package com.juliengrandchavin.schoollocaliser.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.juliengrandchavin.schoollocaliser.R;

public class SettingsActivity extends AppCompatActivity {

    public ImageButton returnButton;
    public ImageButton publicButton;
    public ImageButton privateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        returnButton = findViewById(R.id.return_button);
        returnButton.setOnClickListener(genericOnClickListener);

        publicButton = findViewById(R.id.public_button);
        publicButton.setOnClickListener(genericOnClickListener);

        privateButton = findViewById(R.id.private_button);
        privateButton.setOnClickListener(genericOnClickListener);

        SharedPreferences prefs = getSharedPreferences("settings", MODE_PRIVATE);
        String actualPublicStatus = prefs.getString("public", "");
        String actualPrivateStatus = prefs.getString("private", "");

        switch (actualPublicStatus) {
            case "true":
                publicButton.setAlpha(200);
                break;
            case "false":
                publicButton.setAlpha(70);
                break;
            default:
                publicButton.setAlpha(200);
                break;
        }

        switch (actualPrivateStatus) {
            case "true":
                privateButton.setAlpha(200);
                break;
            case "false":
                privateButton.setAlpha(70);
                break;
            default:
                privateButton.setAlpha(200);
                break;
        }

    }

    // OnClickListener for this view
    View.OnClickListener genericOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.return_button:
                    // Go to MapActivity
                    Intent intent = new Intent(SettingsActivity.this, MenuActivity.class);
                    startActivity(intent);
                    break;
                case R.id.public_button:
                    onSettingsIconClick("public");
                    break;
                case R.id.private_button:
                    onSettingsIconClick("private");
                    break;
            }
        }
    };

    private void onSettingsIconClick (String status) {

        SharedPreferences prefs = getSharedPreferences("settings", MODE_PRIVATE);

        if ("public".equals(status)) {

            String actualStatus = prefs.getString("public", "");
            SharedPreferences.Editor editor = prefs.edit();
            Log.e("public", actualStatus);

            switch (actualStatus) {
                case "true":
                    publicButton.setAlpha(70);
                    editor.putString("public", "false");
                    editor.apply();
                    break;
                case "false":
                    publicButton.setAlpha(200);
                    editor.putString("public", "true");
                    editor.apply();
                    break;
                default:
                    publicButton.setAlpha(70);
                    editor.putString("public", "false");
                    editor.apply();
                    break;
            }

        } else if ("private".equals(status)) {

            String actualStatus = prefs.getString("private", "");
            SharedPreferences.Editor editor = prefs.edit();
            Log.e("private", actualStatus);

            switch (actualStatus) {
                case "true":
                    privateButton.setAlpha(70);
                    editor.putString("private", "false");
                    editor.apply();
                    break;
                case "false":
                    privateButton.setAlpha(200);
                    editor.putString("private", "true");
                    editor.apply();
                    break;
                default:
                    privateButton.setAlpha(70);
                    editor.putString("private", "false");
                    editor.apply();
                    break;
            }

        }

    }
}
