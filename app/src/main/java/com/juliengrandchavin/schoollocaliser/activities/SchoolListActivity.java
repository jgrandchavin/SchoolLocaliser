package com.juliengrandchavin.schoollocaliser.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.juliengrandchavin.schoollocaliser.R;
import com.juliengrandchavin.schoollocaliser.adapters.SchoolListAdapter;
import com.juliengrandchavin.schoollocaliser.interfaces.VolleyCallback;
import com.juliengrandchavin.schoollocaliser.models.School;
import com.juliengrandchavin.schoollocaliser.services.SchoolService;

import java.util.List;

public class SchoolListActivity extends AppCompatActivity {

    public ImageButton returnButton;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private SchoolService schoolService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);

        returnButton = findViewById(R.id.return_button);
        returnButton.setOnClickListener(genericOnClickListener);

        schoolService = new SchoolService(this);
        schoolService.retrieveSchool(new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {
                recyclerView = findViewById(R.id.school_recycle_view);
                final RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                adapter = new SchoolListAdapter(schoolService.SchoolsList, SchoolListActivity.this);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


    }

    // OnClickListener for this view
    View.OnClickListener genericOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        switch (view.getId()) {
            case R.id.return_button:
                // Go to MapActivity
                Intent intent = new Intent(SchoolListActivity.this, MenuActivity.class);
                startActivity(intent);
        }
        }
    };

}
