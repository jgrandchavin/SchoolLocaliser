package com.juliengrandchavin.schoollocaliser.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.juliengrandchavin.schoollocaliser.R;
import com.juliengrandchavin.schoollocaliser.services.AuthenticationService;
import com.juliengrandchavin.schoollocaliser.interfaces.VolleyCallback;

import org.json.*;

public class LoginActivity extends AppCompatActivity {

    // Components declarations
    public Button validateButton;
    public TextView emailTextView;
    public TextView passwordTextView;
    public CheckBox rememberMeCheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Components Initialization
        validateButton = findViewById(R.id.validate_button);
        validateButton.setOnClickListener(genericOnClickListener);
        emailTextView = findViewById(R.id.email_text);
        passwordTextView = findViewById(R.id.password_text);

        SharedPreferences prefs = getSharedPreferences("login", MODE_PRIVATE);

        emailTextView.setText(prefs.getString("email", ""));
        passwordTextView.setText(prefs.getString("password", ""));

        rememberMeCheckbox = findViewById(R.id.rememberMeCheckbox);
    }

    // OnClickListener for this view
    View.OnClickListener genericOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.validate_button:
                    if (TextUtils.isEmpty(emailTextView.getText())){
                        emailTextView.setError("Please enter a username");
                    }
                    if (TextUtils.isEmpty(passwordTextView.getText())){
                        passwordTextView.setError("Please enter a password");
                    }
                    // If all file are not empty, we test the authentication
                    if (!TextUtils.isEmpty(emailTextView.getText()) && !TextUtils.isEmpty(passwordTextView.getText())) {
                        loginIn();
                    }
                    break;
                case R.id.resetButton:
                    emailTextView.setText("");
                    passwordTextView.setText("");
            }
        }
    };

    private void loginIn () {
        new AuthenticationService(this).authenticateUser(emailTextView.getText().toString(), passwordTextView.getText().toString(), new VolleyCallback() {
            @Override
            public void onSuccessResponse(String result) {

                try {
                    JSONObject response = new JSONObject(result);
                    String authToken = response.getString("auth_token");
                    SharedPreferences sharedPreferences = getBaseContext().getApplicationContext().getSharedPreferences("login", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("auth_token", authToken);
                    editor.apply();
                    if (rememberMeCheckbox.isChecked()) {
                        SharedPreferences.Editor editor2 = sharedPreferences.edit();
                        editor2.putString("email", emailTextView.getText().toString());
                        editor2.putString("password", passwordTextView.getText().toString());
                        editor2.apply();
                    } else {
                        SharedPreferences.Editor editor2 = sharedPreferences.edit();
                        editor2.putString("email", "");
                        editor2.putString("password", "");
                        editor2.apply();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(intent);
            }
            @Override
            public void onErrorResponse (VolleyError error) {
                Toast.makeText(getBaseContext(), "Authentification raté ", Toast.LENGTH_LONG).show();
            }
        });


    }
}
