package com.juliengrandchavin.schoollocaliser.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.juliengrandchavin.schoollocaliser.R;
import com.juliengrandchavin.schoollocaliser.interfaces.VolleyCallback;
import com.juliengrandchavin.schoollocaliser.models.School;
import com.juliengrandchavin.schoollocaliser.services.SchoolService;

public class AddToListActivity extends AppCompatActivity {

    public ImageButton returnButton;
    public Spinner spinner;

    public TextView nameText;
    public TextView addressText;
    public TextView zipcodeText;
    public TextView cityText;
    public TextView openingHoursText;
    public TextView phoneNumberText;
    public TextView emailText;
    public TextView longitudeText;
    public TextView latitudeText;
    public TextView studentsCountText;

    public Button validateButton;

    private SchoolService schoolService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_list);

        schoolService = new SchoolService(this);

        returnButton = findViewById(R.id.return_button);
        returnButton.setOnClickListener(genericOnClickListener);

        nameText = findViewById(R.id.name_text);
        addressText = findViewById(R.id.address_text);
        zipcodeText = findViewById(R.id.zipcode_text);
        cityText = findViewById(R.id.city_text);
        openingHoursText = findViewById(R.id.opening_hours_text);
        phoneNumberText = findViewById(R.id.phone_text);
        emailText = findViewById(R.id.email_text);
        latitudeText = findViewById(R.id.latitude_text);
        longitudeText = findViewById(R.id.longitude_text);
        studentsCountText = findViewById(R.id.students_count_text);

        validateButton = findViewById(R.id.validate_button);
        validateButton.setOnClickListener(genericOnClickListener);

        spinner = findViewById(R.id.hand_spinner);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(this,
                R.array.school_status_array, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    // OnClickListener for this view
    View.OnClickListener genericOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.return_button:
                    // Go to MapActivity
                    Intent intent = new Intent(AddToListActivity.this, MenuActivity.class);
                    startActivity(intent);
                    break;
                case R.id.validate_button:

                    if (TextUtils.isEmpty(nameText.getText())){
                        nameText.setError("Please enter a name");
                    }
                    if (TextUtils.isEmpty(emailText.getText())){
                        emailText.setError("Please enter a email");
                    }
                    if (TextUtils.isEmpty(latitudeText.getText())){
                        latitudeText.setError("Please enter a latitude");
                    }
                    if (TextUtils.isEmpty(longitudeText.getText())){
                        longitudeText.setError("Please enter a longitude");
                    }
                    if (TextUtils.isEmpty(studentsCountText.getText())){
                        studentsCountText.setError("Please enter a students counts");
                    }

                    // If all file are not empty, we test the authentication
                    if (!TextUtils.isEmpty(emailText.getText()) && !TextUtils.isEmpty(nameText.getText()) && !TextUtils.isEmpty(latitudeText.getText()) && !TextUtils.isEmpty(longitudeText.getText()) && !TextUtils.isEmpty(studentsCountText.getText())){
                        schoolService.addNewSchool(constructNewSchool(), new VolleyCallback(){

                            @Override
                            public void onSuccessResponse(String result) {
                                Intent intent = new Intent(AddToListActivity.this, MenuActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getBaseContext(), "Erreur: la création n'a pas aboutie.. ", Toast.LENGTH_LONG).show();
                            }
                        });
                    }

                    break;
            }
        }
    };

    private School constructNewSchool() {

        return new School(
                0,
                nameText.getText().toString(),
                addressText.getText().toString(),
                zipcodeText.getText().toString(),
                cityText.getText().toString(),
                openingHoursText.getText().toString(),
                phoneNumberText.getText().toString(),
                emailText.getText().toString(),
                Double.valueOf(latitudeText.getText().toString()),
                Double.valueOf(longitudeText.getText().toString()),
                Integer.valueOf(studentsCountText.getText().toString()),
                spinner.getSelectedItem().toString());
    };
}
