package com.juliengrandchavin.schoollocaliser.models;

import java.nio.file.Path;

/**
 * Created by juliengrand-chavin on 06/06/2018.
 */

public class School {

    private int Id;
    private String Name;
    private String Address;
    private String ZipCode;
    private String City;
    private String OpeningHours;
    private String Phone;
    private String Email;
    private double Latitude;
    private double Longitude;
    private int StudentsCount;
    private String Status;

    public School (int id, String name, String address, String zipCode, String city, String openingHours, String phone, String email, double latitude, double longitude, int studentsCount, String status) {
        Id = id;
        Name = name;
        Address = address;
        ZipCode = zipCode;
        City = city;
        OpeningHours = openingHours;
        Phone = phone;
        Email = email;
        Latitude = latitude;
        Longitude = longitude;
        StudentsCount = studentsCount;
        Status = status;
    }

    public int getId() {
        return Id;
    }
    public String getName() {
        return Name;
    }
    public String getAddress() {
        return Address;
    }
    public String getZipCode() {
        return ZipCode;
    }
    public String getCity() {
        return City;
    }
    public String getOpeningHours() {
        return OpeningHours;
    }
    public String getPhone() {
        return Phone;
    }
    public String getEmail() {
        return Email;
    }
    public double getLatitude() {
        return Latitude;
    }
    public double getLongitude() {
        return Longitude;
    }
    public int getStudentsCount() {
        return StudentsCount;
    }
    public String getStatus() {
        return Status;
    }

}
