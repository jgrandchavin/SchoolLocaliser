package com.juliengrandchavin.schoollocaliser.interfaces;

import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by juliengrand-chavin on 05/06/2018.
 */

public interface VolleyCallback {
    void onSuccessResponse(String result);
    void onErrorResponse(VolleyError error);
}
