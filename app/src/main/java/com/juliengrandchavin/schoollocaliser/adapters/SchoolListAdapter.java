package com.juliengrandchavin.schoollocaliser.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.juliengrandchavin.schoollocaliser.R;
import com.juliengrandchavin.schoollocaliser.activities.AddToListActivity;
import com.juliengrandchavin.schoollocaliser.activities.LoginActivity;
import com.juliengrandchavin.schoollocaliser.activities.MapActivity;
import com.juliengrandchavin.schoollocaliser.activities.MenuActivity;
import com.juliengrandchavin.schoollocaliser.activities.SchoolListActivity;
import com.juliengrandchavin.schoollocaliser.interfaces.VolleyCallback;
import com.juliengrandchavin.schoollocaliser.models.School;
import com.juliengrandchavin.schoollocaliser.services.SchoolService;


import java.util.List;

/**
 * Created by juliengrand-chavin on 05/06/2018.
 */

public class SchoolListAdapter extends RecyclerView.Adapter<ViewHolder> implements LocationListener{

    private List<School> schoolsList;
    private Activity activity;
    private SchoolService schoolService;
    private School school;
    private double lon;
    private double lat;


    public SchoolListAdapter(List<School> schoolsList, Activity activity) {
        this.schoolsList = schoolsList;
        this.activity = activity;
        schoolService = new SchoolService(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_school_list, parent, false);

        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        int orange = ContextCompat.getColor(activity, R.color.Orange);
        int green = ContextCompat.getColor(activity, R.color.Green);
        int red = ContextCompat.getColor(activity, R.color.Red);

        school = schoolsList.get(position);


        float[] distanceArray = new float[1];
        Location.distanceBetween(4.85,45.75, school.getLongitude(), school.getLatitude(), distanceArray);
        String distance = String.valueOf( Math.round(distanceArray[0]/1000) + " KM");

        holder.distanceText.setText(distance);

        holder.name.setText(school.getName());
        holder.address.setText(school.getAddress());
        holder.studentCount.setText(new StringBuilder().append(String.valueOf(school.getStudentsCount())).append(" élèves").toString());

        holder.mapButton.setOnClickListener(genericOnClickListener);
        holder.deleteButton.setOnClickListener(genericOnClickListener);


        if (school.getStudentsCount() < 50) {
            holder.background.setBackground(new ColorDrawable(red));
            holder.thumbImage.setImageResource(R.drawable.thumb_down);
        } else if (school.getStudentsCount() >= 50 && school.getStudentsCount() < 200) {
            holder.background.setBackground(new ColorDrawable(orange));
            holder.thumbImage.setImageResource(R.drawable.thumb_up);
        } else {
            holder.background.setBackground(new ColorDrawable(green));
            holder.thumbImage.setImageResource(R.drawable.thumb_up);
        }

    }

    @Override
    public int getItemCount() {
        return schoolsList.size();
    }

    // OnClickListener for this view
    View.OnClickListener genericOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.map_button:
                    Intent intent = new Intent(activity, MapActivity.class);
                    activity.startActivity(intent);
                    break;
                case R.id.delete_button:
                    schoolService.deleteSchool(school.getId(), new VolleyCallback() {
                        @Override
                        public void onSuccessResponse(String result) {
                            Toast.makeText(activity, "Suppression réussi ! ", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(activity, MenuActivity.class);
                            activity.startActivity(intent);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(activity, "Suppression ratée... ", Toast.LENGTH_LONG).show();
                        }
                    });
                    break;

            }
        }
    };

    @Override
    public void onLocationChanged(Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
