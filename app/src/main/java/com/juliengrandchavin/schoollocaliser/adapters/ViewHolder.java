package com.juliengrandchavin.schoollocaliser.adapters;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.juliengrandchavin.schoollocaliser.R;

/**
 * Created by juliengrand-chavin on 06/06/2018.
 */

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView name;
    public ConstraintLayout background;
    public ImageView thumbImage;
    public TextView address;
    public TextView studentCount;
    public ImageButton mapButton;
    public ImageButton deleteButton;
    public TextView distanceText;

    public ViewHolder(View view) {
        super(view);
        name = view.findViewById(R.id.school_name);
        background = view.findViewById(R.id.background);
        thumbImage = view.findViewById(R.id.thumb_image);
        address = view.findViewById(R.id.school_address);
        studentCount = view.findViewById(R.id.students_count);
        mapButton = view.findViewById(R.id.map_button);
        deleteButton = view.findViewById(R.id.delete_button);
        distanceText = view.findViewById(R.id.distance_text);


    }
}
