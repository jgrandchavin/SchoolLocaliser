package com.juliengrandchavin.schoollocaliser.services;

import android.app.Activity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.juliengrandchavin.schoollocaliser.interfaces.VolleyCallback;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class AuthenticationService {

    private WeakReference<Activity> loginActivity;

    public AuthenticationService (Activity activity)  {
        this.loginActivity = new WeakReference<Activity>(activity);
    }

    /**
     * Authenticate user and put auth_token in cache
     *  @param email current user email
     *  @param password current user password
     *
     * */
    public void authenticateUser (final String email, final String password, final VolleyCallback callback) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.loginActivity.get());

        StringRequest postRequest = new StringRequest(Request.Method.POST, "https://schoolz-api.herokuapp.com/api/v1/users/sign_in",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccessResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                       callback.onErrorResponse(error);
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }
        };
        queue.add(postRequest);
    }

}
