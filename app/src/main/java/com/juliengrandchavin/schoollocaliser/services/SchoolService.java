package com.juliengrandchavin.schoollocaliser.services;

import android.app.Activity;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.juliengrandchavin.schoollocaliser.interfaces.VolleyCallback;
import com.juliengrandchavin.schoollocaliser.models.School;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by juliengrand-chavin on 05/06/2018.
 */


public class SchoolService {

    public List<School> SchoolsList = new ArrayList<>();

    private WeakReference<Activity> activity;

    public SchoolService (Activity activity)  {
        this.activity = new WeakReference<Activity>(activity);
    }

    public void retrieveSchool (final VolleyCallback callback) {

        SharedPreferences prefs = activity.get().getSharedPreferences("settings", MODE_PRIVATE);
        String actualPublicStatus = prefs.getString("public", "");
        String actualPrivateStatus = prefs.getString("private", "");
        String url = "https://schoolz-api.herokuapp.com/api/v1/schools";

        if (actualPublicStatus.equals("true")) {
            url = url + "?status=public";
        }
        if (actualPrivateStatus.equals("true")) {
            url = url + "?status=private";
        }
        if (actualPublicStatus.equals("true") && actualPrivateStatus.equals("true")) {
            url = "https://schoolz-api.herokuapp.com/api/v1/schools";
        }

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.activity.get());

        StringRequest postRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);

                            convertSchoolJsonArrayToSchoolList(jsonResponse.getJSONArray("schools"));

                            callback.onSuccessResponse(response);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(activity.get(), "La récupération des écolese échoué", Toast.LENGTH_LONG).show();
                        callback.onErrorResponse(error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders()
            {
                Map<String, String>  params = new HashMap<String, String>();
                SharedPreferences prefs = activity.get().getSharedPreferences("login", MODE_PRIVATE);
                params.put("AUTHORIZATION", prefs.getString("auth_token", ""));
                return params;
            }
        };
        queue.add(postRequest);

    }

    public void addNewSchool (final School newSchool, final VolleyCallback callback) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.activity.get());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "https://schoolz-api.herokuapp.com/api/v1/schools",
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                       callback.onSuccessResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onErrorResponse(error);
                    }
                }
        ) {

            @Override
            protected Map<String, String> getParams()  {
                Map<String, String> params = new HashMap<>();


                params.put("name", newSchool.getName());
                params.put("address", newSchool.getAddress());
                params.put("zip_code", newSchool.getZipCode());
                params.put("city", newSchool.getCity());
                params.put("opening_hours", newSchool.getOpeningHours());
                params.put("email", newSchool.getEmail());
                params.put("latitude", String.valueOf(newSchool.getLatitude()));
                params.put("longitude", String.valueOf(newSchool.getLongitude()));
                params.put("students_count", String.valueOf(newSchool.getStudentsCount()));
                params.put("status", newSchool.getStatus());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                SharedPreferences prefs = activity.get().getSharedPreferences("login", MODE_PRIVATE);
                params.put("AUTHORIZATION", prefs.getString("auth_token", ""));
                params.put("Content-Type", "application/json");
                return params;
            }
        };

        queue.add(stringRequest);

    }

    public void deleteSchool (int schoolId, final VolleyCallback callback) {

        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this.activity.get());

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, "https://schoolz-api.herokuapp.com/api/v1/schools/" + Integer.toString(schoolId),
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        callback.onSuccessResponse(response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onErrorResponse(error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String>  params = new HashMap<>();
                SharedPreferences prefs = activity.get().getSharedPreferences("login", MODE_PRIVATE);
                params.put("AUTHORIZATION", prefs.getString("auth_token", ""));
                return params;
            }
        };

        queue.add(stringRequest);



    }

    private void convertSchoolJsonArrayToSchoolList (JSONArray jsonArray) {

        for (int i = 0 ; i < jsonArray.length() ; i++)  {

            try {
                JSONObject school = (JSONObject) jsonArray.get(i);

                this.SchoolsList.add(new School(
                        school.getInt("id"),
                        school.getString("name"),
                        school.getString("address"),
                        school.getString("zip_code"),
                        school.getString("city"),
                        school.getString("opening_hours"),
                        school.getString("phone"),
                        school.getString("email"),
                        school.getDouble("latitude"),
                        school.getDouble("longitude"),
                        school.getInt("students_count"),
                        school.getString("status")
                ));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
